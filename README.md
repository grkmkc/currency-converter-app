## Developer Notes

- This project contains a reusable custom Currency converter with free API. It's just dependant on some custom components and some constant values.
- I just wanted this project to be looks like a part of the real production project. So I decided to go with a very similar folder structure that I use on production too. Of course, there are so many things to add for example UI lib, redux, or different state management approaches, etc...
- Since we are using the UI library that we wrote and actively maintaining as a team, I didn't want to use any UI lib. and just used styled-components for styling.

### You will find the necessary project scripts, expectations and planned/spent time below;

### Expected and working user/test cases

- On page load latest currencies will be set from API. And EUR will be the base currency due to free API restrictions.

- User can select target currency. (On page load it will be set for the first object )

- User can see the ratio between base and target currency.

- User can fill the amount field with number or decimals (supporting --> "," and ".")

- User can select the date for historical ratios.

- On-page load API will be triggered once but then if the user clicks the `convert` button without changing the date, the API request will not be triggered again. So I expect no API request if the date same as the last one.

- Responsive design implemented for the devices under desktop size. For the devices over desktop size, I did simply set the exact width.

- As I mentioned in the first element, I didn't change the base currency due to free API restrictions. But I wrote the code like I could change the base.

### Scheculed Time

- Since I am currently working as a developer, the time I can spare was limited, so I decided to go with some time frames that I found after work. So as you see below I noted these time frames.

  - 0-2 hours --> Basic project structure and basic layout with styled-components and resetters.,
  - 2-4 hours --> UI improvements and some functionality added, reusable utils like converter, custom hook for fetching data
  - 4-6 hours --> UI and functionality wrap up.
  - 6~ hours --> Some test cases and documentation
  - Deploy process.

### Features I have been thinking about and planning but couldn't include

- Time-based diagrams and charts.
- Two-way target changing, as I mentioned before.
- Redux/saga and more product-like arch.
- Tests
- Better services layer with its utils and error handling on the services layer
- Some UI improvements like error toats messages.
- I would like to improve or think on the better and optimized requests.

### Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
