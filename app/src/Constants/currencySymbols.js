export const CurrencySymbols = [
  {
    key: "USD",
    value: "USD",
  },
  {
    key: "AUD",
    value: "AUD",
  },
  {
    key: "CAD",
    value: "CAD",
  },
  {
    key: "PLN",
    value: "PLN",
  },
  {
    key: "MXN",
    value: "MXN",
  },
];
