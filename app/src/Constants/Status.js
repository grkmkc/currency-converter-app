export const Status = {
  INIT: 1,
  PENDING: 2,
  LOADED: 3,
  FAILED: 4,
};
