import React from "react";
import CurrencyConverter from "Containers/CurrencyConverter";

import * as S from "./style";
function Main() {
  return (
    <S.MainSection>
      <CurrencyConverter></CurrencyConverter>
    </S.MainSection>
  );
}

export default Main;
