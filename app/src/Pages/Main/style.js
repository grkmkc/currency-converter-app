import styled from "styled-components";
import { BREAKPOINT_MD } from "Constants/Breakpoints";

const MainSection = styled.section`
  display: flex;
  overflow: hidden;
  padding: 20px;
  max-width: 1368px;
  height: 400px;
  margin: 0 auto;

  @media (max-width: ${BREAKPOINT_MD}px) {
    padding: 0px;
    height: 100%;
  }
`;

export { MainSection };
