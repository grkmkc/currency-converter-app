export const simplyDate = (timestamp) => {
  var date = new Date(timestamp * 1000);

  return date.toDateString();
};
