export const converter = (amount, value1, value2, rateValue) => {
  const rate = rateValue.key || rateValue;
  const secondaryRatio = (1 / rate).toFixed(4);
  const convertedValue = amount !== 0 && (amount * rate).toFixed(6);

  const renderTextFirst = `1 ${value1} = ${rate} ${value2}`;
  const renderTextSecond = `1 ${value2} = ${secondaryRatio} ${value1}`;

  const renderResult =
    amount !== 0 && `${amount} ${value1} = ${convertedValue} ${value2}`;

  return { renderTextFirst, renderTextSecond, renderResult };
};
