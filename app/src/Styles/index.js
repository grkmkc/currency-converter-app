import React from "react";
import { createGlobalStyle } from "styled-components";
import { normalize } from "styled-normalize";
import typography from "./typography";
import variables from "./variables";
import reset from "./reset";

const NormalizeGlobalStyle = createGlobalStyle`
  ${normalize}`;
const TypographyGlobalStyle = createGlobalStyle`
  ${typography}`;
const VariablesGlobalStyle = createGlobalStyle`
  ${variables}`;
const ResetGlobalStyle = createGlobalStyle`
  ${reset}`;

function GlobalStyle() {
  return (
    <>
      <NormalizeGlobalStyle />
      <TypographyGlobalStyle />
      <VariablesGlobalStyle />
      <ResetGlobalStyle />
    </>
  );
}

export default GlobalStyle;
