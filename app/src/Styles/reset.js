import { css } from "styled-components";

export default css`
  html,
  body,
  #root {
    height: 100%;
  }

  .App {
    height: 100%;
    background: rgb(2, 0, 36);
    background: linear-gradient(
      0deg,
      rgba(2, 0, 36, 1) 0%,
      rgba(2, 4, 54, 0.47942927170868344) 100%,
      rgba(0, 195, 239, 1) 100%,
      rgba(0, 212, 255, 1) 100%
    );
  }
`;
