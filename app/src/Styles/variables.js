import { css } from "styled-components";

export default css`
  :root {
    /* Colors */
    --color-default: #fff;
    --color-primary: #0abf53;
    --color-dark-primary: #078f3e;
    --color-bg-primary: #f3f6f9;
    --color-bg-secondary: #031033;
    --color-secondary: #37506e;
    --color-lightest-gray: #f9fafb;
    --color-lighter-gray: #eceef4;
    --color-gray: #cdd0e0;
    --color-darker-gray: #6e7492;
    --color-darkest-gray: #191d2f;

    /* Font Weights */
    --font-weight-regular: 400;
    --font-weight-medium: 500;
    --font-weight-bold: 600;

    /* Font Sizes */
    --font-size-small: 12px;
    --font-size-medium: 14px;
    --font-size-large: 16px;
  }
`;
