import React, { useState, useEffect } from "react";

import useFetch from "Hooks/useFetch";
import CustomInput from "Components/CustomInput";
import CustomSelect from "Components/CustomSelect";
import CustomButton from "Components/CustomButton";
import CustomDateSelect from "Components/CustomDateSelect";
import { BASE_URL } from "Services/currencyService/constants";
import { API_KEY } from "config";
// import { dummyResponse as currencyRates } from "./constants";
import * as S from "./style";
import { converter } from "Utils/converter";
import { simplyDate } from "Utils/simplyDate";

let currencyRates;
function CurrencyCoverter() {
  const [inputValue, setInputValue] = useState(0);
  const [historicalDate, setHistoricalDate] = useState(
    new Date().toISOString().slice(0, 10),
  );
  const [selectedDate, setSelectedDate] = useState(
    new Date().toISOString().slice(0, 10),
  );
  const [convertedResult, setConvertedResult] = useState("");
  const [currencyFirst, setCurrencyFirst] = useState("");
  const [currencySecond, setCurrencySecond] = useState("");
  const [currencySymbols, setCurrencySymbols] = useState([]);
  const [isRatesLoaded, setRatesLoaded] = useState(false);

  currencyRates = useFetch(`${BASE_URL}/latest?access_key=${API_KEY}`, {});

  useEffect(() => {
    if (!currencySymbols.length && currencyRates.response) {
      handleDataConversion();
    }
  }, [currencyRates]);

  useEffect(() => {
    isRatesLoaded && handleConvertedValue();
  }, [isRatesLoaded, currencyRates]);

  const handleConvertedValue = () => {
    const result = converter(
      inputValue,
      currencyFirst,
      currencySecond,
      currencySymbols[
        currencySymbols.findIndex((x) => x.value == currencySecond)
      ],
    );
    const rawHtml = `
     <div>${result.renderTextFirst}</div>
     <br>
     <div>${result.renderTextSecond}</div>
     <br>
     <div>${result.renderResult ? result.renderResult : ""}</div>
     <br>
    `;
    setConvertedResult(rawHtml);
  };

  const handleInputChange = (event) => {
    if (/^[+-]?\d*(?:[.,]\d*)?$/.test(event.target.value))
      setInputValue(event.target.value);
  };

  const handleButtonClicked = async () => {
    if (!historicalDate) {
      handleConvertedValue();
    } else if (historicalDate && selectedDate !== historicalDate) {
      const response = await fetch(
        `${BASE_URL}/${historicalDate}?access_key=${API_KEY}`,
      );
      const result = await response.json();
      if (result) {
        currencyRates = { error: null, isLoading: false, response: result };
        handleDataConversion();
        setSelectedDate(historicalDate);
      }
    }
  };

  const handleCurrencyChange = (value, type) => {
    type === "currencyFirst"
      ? setCurrencyFirst(value)
      : setCurrencySecond(value);
  };

  const handleDataConversion = () => {
    setCurrencyFirst(currencyRates.response.base);

    const rates = currencyRates.response.rates;
    const currencyArray = Object.keys(rates).map((value, key) => {
      return { value: value, key: rates[value] };
    });
    setCurrencySymbols(currencyArray);
    setCurrencySecond(currencyArray[0].value);
    setRatesLoaded(true);
  };

  if (!currencyRates.response) {
    return <div>Loading...</div>;
  }

  return (
    <S.CardWrapper>
      <S.CardContent>
        <CustomInput
          label='Amount'
          pattern='[+-]?\d+(?:[.,]\d+)?'
          onChange={handleInputChange}
          value={inputValue}
          placeholder='Enter Amount'
        />
        <CustomSelect
          label='FROM'
          value={currencyFirst}
          onChange={(event) =>
            handleCurrencyChange(event.target.value, "currencyFirst")
          }
        >
          <option value='EUR' key='EUR'>
            EUR
          </option>
          {/* {currencySymbols.map(({ value }) => (
            <option value={value} key={value}>
              {value}
            </option>
          ))} */}
        </CustomSelect>
        <CustomSelect
          label='TO'
          value={currencySecond}
          onChange={(event) =>
            handleCurrencyChange(event.target.value, "currencySecond")
          }
        >
          {currencySymbols.map(({ value }) => (
            <option value={value} key={value}>
              {value}
            </option>
          ))}
        </CustomSelect>
      </S.CardContent>
      <S.CardContent>
        <CustomDateSelect
          defaultValue={new Date().toISOString().slice(0, 10)}
          max={new Date().toISOString().slice(0, 10)}
          label='Select Date'
          placeholder='YYYY-MM-DD'
          onChange={(e) => setHistoricalDate(e.target.value)}
        />
      </S.CardContent>

      <S.CardFooter>
        <S.ResultWrapper>
          {
            <div
              dangerouslySetInnerHTML={{
                __html: convertedResult,
              }}
            />
          }
          <div>Last updated {simplyDate(currencyRates.response.timestamp)}</div>
        </S.ResultWrapper>
        <CustomButton onClick={handleButtonClicked}>Convert</CustomButton>
      </S.CardFooter>
    </S.CardWrapper>
  );
}

export default CurrencyCoverter;
