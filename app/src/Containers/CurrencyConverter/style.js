import styled from "styled-components";
import { BREAKPOINT_MD } from "Constants/Breakpoints";

const CardWrapper = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  overflow: hidden;
  padding: 20px;
  margin: 32px;
  box-shadow: 0 0 0px rgb(0 0 0 / 5%), 0 0px 5px rgb(0 0 0 / 10%);
  border-radius: 4px;
  background-color: var(--color-bg-primary);

  @media (max-width: ${BREAKPOINT_MD}px) {
    margin: 16px;
    padding: 4px;
  }
`;

const CardContent = styled.div`
  display: flex;
  flex: 1;
  justify-content: space-between;
  padding: 8px;
  @media (max-width: ${BREAKPOINT_MD}px) {
    flex-direction: column;
    justify-content: flex-start;
  }
`;

const CardFooter = styled.div`
  display: flex;
  flex: 1;
  flex-direction: row;
  justify-content: space-between;
  padding: 8px;

  @media (max-width: ${BREAKPOINT_MD}px) {
    flex-direction: column-reverse;
    justify-content: flex-start;
    align-items: center;
  }
`;

const ResultWrapper = styled.div`
  display: flex;
  flex-direction: column;
  line-height: 1.25em;
  font-weight: var(--font-weight-bold);
  font-size: var(--font-size-large);

  @media (max-width: ${BREAKPOINT_MD}px) {
    flex: 0;
  }
`;

export { CardWrapper, CardContent, CardFooter, ResultWrapper };
