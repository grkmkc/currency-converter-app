import styled from "styled-components";
import { BREAKPOINT_MD } from "Constants/Breakpoints";

const Button = styled.button`
  display: block;
  width: 100%;
  font-size: var(--font-size-medium);
  font-weight: var(--font-weight-bold);
  padding: 2px;
  background-color: var(--color-primary);
  border: 1px solid var(--color-primary);
  border-radius: 8px;
  cursor: pointer;
  color: var(--color-default);
  transition: all 0.25s linear;
  max-width: 175px;
  height: 48px;

  &:hover {
    background-color: var(--color-dark-primary);
  }

  @media (max-width: ${BREAKPOINT_MD}px) {
    max-width: 100%;
    margin-bottom: 16px;
  }
`;

export { Button };
