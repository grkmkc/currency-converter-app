import React from "react";

import * as S from "./style";
function CustomButton({ children, ...props }) {
  return <S.Button {...props}>{children}</S.Button>;
}

export default CustomButton;
