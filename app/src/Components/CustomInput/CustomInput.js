import React from "react";

import * as S from "./style";

function CustomInput(props) {
  return (
    <S.Wrapper>
      <S.CustomLabel>{props.label || "Label"}</S.CustomLabel>
      <S.CustomInput {...props} />
    </S.Wrapper>
  );
}

export default CustomInput;
