import styled from "styled-components";
import { BREAKPOINT_MD } from "Constants/Breakpoints";

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  margin: 0 8px;
  @media (max-width: ${BREAKPOINT_MD}px) {
    margin: 16px 0px;
  }
`;

const CustomInput = styled.input`
  padding: 4px 4px;
  max-width: 100%;
  font-size: var(--font-size-medium);
  min-height: 28px;
  border: 1px solid var(--color-darker-gray);
  border-radius: 4px;

  &:focus {
    border-color: var(--color-primary);
    outline: 0;
  }
`;

const CustomLabel = styled.label`
  margin-bottom: 8px;
`;

export { CustomInput, Wrapper, CustomLabel };
