import {
  Wrapper as CustomWrapper,
  CustomLabel,
} from "Components/CustomInput/style";
import styled from "styled-components";
import { BREAKPOINT_XL } from "Constants/Breakpoints";

const DateInput = styled.input`
  padding: 4px 4px;
  max-width: 100%;
  font-size: var(--font-size-medium);
  min-height: 28px;
  border: 1px solid var(--color-darker-gray);
  border-radius: 4px;
`;

const Wrapper = styled(CustomWrapper)`
  width: 400px;
  @media (max-width: ${BREAKPOINT_XL}px) {
    width: 100%;
  }
`;
export { Wrapper, CustomLabel, DateInput };
