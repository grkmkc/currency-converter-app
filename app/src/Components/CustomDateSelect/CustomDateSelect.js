import React from "react";

import * as S from "./style";

function CustomDateSelect(props) {
  return (
    <S.Wrapper>
      <S.CustomLabel>{props.label || "Label"}</S.CustomLabel>
      <S.DateInput type='date' {...props} />
    </S.Wrapper>
  );
}

export default CustomDateSelect;
