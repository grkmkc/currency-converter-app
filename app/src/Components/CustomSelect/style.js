import styled from "styled-components";
import { BREAKPOINT_MD } from "Constants/Breakpoints";

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  margin: 0 8px;

  @media (max-width: ${BREAKPOINT_MD}px) {
    margin: 16px 0px;
  }
`;

const CustomSelect = styled.select`
  border-radius: 4px;
  border: 1px solid var(--color-darker-gray);
  padding: 0;
  max-width: 100%;
  font-size: var(--font-size-medium);
  min-height: 40px;
  max-height: 45px;

  &:focus {
    border-color: var(--color-primary);
    outline: 0;
  }
`;

const CustomLabel = styled.label`
  margin-bottom: 8px;
`;

export { CustomSelect, Wrapper, CustomLabel };
