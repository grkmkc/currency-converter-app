import React from "react";

import * as S from "./style";

function CustomSelect({ children, ...props }) {
  return (
    <S.Wrapper>
      <S.CustomLabel>{props.label || "Label"}</S.CustomLabel>
      <S.CustomSelect {...props}>{children}</S.CustomSelect>
    </S.Wrapper>
  );
}

export default CustomSelect;
